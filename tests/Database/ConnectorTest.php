<?php

namespace tests\Database;

use Api\V1\Hotel\RoomController;

class ConnectorTest extends \PHPUnit\DbUnit\TestCase {

    protected $object;
    static private $pdo = null;
    private $conn = null;

    protected function setUp() {
        $this->object = new RoomController;
    }

    protected function getConnection() {
        if ($this->conn === null) {
            if (self::$pdo == null) {
                self::$pdo = new \PDO('mysql:dbname=restapi;host=localhost', 'root', 'admin');
            }
            $this->conn = $this->createDefaultDBConnection(self::$pdo, 'restapi');
        }
        return $this->conn;
    }

    protected function getDataSet() {
        return $this->createMySQLXMLDataSet(__DIR__ . '/../_files/acc.xml');
    }

    public function testDataBaseConnection() {

        $this->getConnection()->createDataSet(array('acc'));
        $this->getDataSet();
        $queryTable = $this->getConnection()
                ->createQueryTable(
                'acc', 'SELECT room_id, survey_id, host_id, room_type, country, city, borough, neighborhood, reviews, overall_satisfaction, accommodates, bedrooms, bathrooms, price, minstay, last_modified, latitude, longitude, ST_AsText(location) as location FROM acc'
        );
        $expectedTable = $this->getDataSet()->getTable('acc');
        $this->assertTablesEqual($expectedTable, $queryTable);
    }

}
