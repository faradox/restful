# RESTful
Custom restful API.


## Requirements

- [PHP](http://www.php.net) >= 7.0 **with** [cURL](http://www.php.net/manual/en/curl.installation.php)

## Installation

You can install using 
```bash
$ git clone git clone https://faradox@bitbucket.org/faradox/restful.git
```
Then

```bash
$ composer install
```
##Branch

Secondary branch

https://bitbucket.org/faradox/restful/src/bee56cf04f1fdfc8d1cdd71b3d5be627a3603159/?at=secondary

## Settings

Document root must be like path/to/project/api

By default response will be returned in JSON format, if you want to get response in xml, send appropriate header.

## Usage

1. Get 100 rows by default,

```bash
$ curl -i -G http://yourserver/v1/hotel/room
```

You can filter by room type using parameter filterByRoomType


```bash
$ curl -i -G http://yourserver/v1/hotel/room/index/filterByRoomType/Shared%20room
```

2. Get certain entity by room id. You can send header in order to get desired format

```bash
$ curl -i -H "Accept:application/xml" http://yourserver/v1/hotel/room/get/id/9404241
```

3. POST an object into db


```bash
$ curl -i -X POST -H "Accept:application/json" -H "Content-Type:application/json" -d '{"room_id": 9404235,"survey_id": 1265,"host_id": 34546002,"room_type": "Shared room","country": null,"city": "Las Vegas farad","borough": null,   "neighborhood": "42","reviews": 10,"overall_satisfaction": 5,"accommodates": 16,"bedrooms": "1.00","bathrooms": null,"price": 75,"minstay": null,"latitude": "36.120369","longitude": "-115.212566"}' http://yourserver/v1/hotel/room/store
```


4. Deleting entity from db requires Http basic authorization, we will send Http Authorization header with user:pass encoded with base64

```bash
$ curl -i -X DELETE -H "Content-Type:application/json" -H "Authorization: Basic dXNlcjpwYXNz" -d '{"room_id":9397616}' http://yourserver/v1/hotel/room/delete
```

### Test

1. Checking DB connection
```bash
$ vendor/bin/phpunit

```


