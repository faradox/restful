<?php

namespace Api\V1\Hotel;

use Core\Routing\Controller;
use Core\Http\Request;

class RoomController extends Controller {

    public function index(Request $request, $filterBy = null) {

        if (!$request->isMethod('get')) {
            $this->makeResponse("Method Not Allowed", 405);
        }
        
        $columns = [
            'room_id',
            'survey_id',
            'host_id',
            'room_type',
            'country',
            'city',
            'borough',
            'neighborhood',
            'reviews',
            'overall_satisfaction',
            'accommodates',
            'bedrooms',
            'bathrooms',
            'price',
            'minstay',
            'last_modified',
            'latitude',
            'longitude',
            'ST_AsText(location) as location'
        ];

        $query = ( new \Api\V1\Hotel\Room())
                ->select($columns);

        if ($filterBy) {
            $query->where($filterBy);
            $result = $query->query();
        } else {
            $result = $query
                    ->order("last_modified DESC")
                    ->limit(100)
                    ->query();
        }

        $this->makeResponse($result);
    }

    public function get(Request $request, $roomId) {
        if (!$roomId || !$request->isMethod('get')) {
            $this->makeResponse("Method Not Allowed", 405);
        }
        $columns = [
            'room_id',
            'survey_id',
            'host_id',
            'room_type',
            'country',
            'city',
            'borough',
            'neighborhood',
            'reviews',
            'overall_satisfaction',
            'accommodates',
            'bedrooms',
            'bathrooms',
            'price',
            'minstay',
            'last_modified',
            'latitude',
            'longitude',
            'ST_AsText(location) as location'
        ];
        $query = ( new \Api\V1\Hotel\Room())
                ->select($columns)
                ->where(["room_id" => $roomId])
                ->query();
        $this->makeResponse($query);
    }

    public function store(Request $request) {
        if (!$request->isMethod('post')) {
            $this->makeResponse("Method Not Allowed", 405);
        }
        if (!$input = $request->getInput()) {
            $this->makeResponse("No input data", 422);
        }

        $input = array_merge($input, ["location" => "ST_GeomFromText('POINT({$input['latitude']} {$input['longitude']})')"]);

        try {
            $stmt = ( new \Api\V1\Hotel\Room())
                    ->insert($input);
            $this->makeResponse("Created", 201);
        } catch (\PDOException $ex) {
            $this->makeResponse($ex->getMessage());
        }
    }

    public function delete(Request $request) {

        if (!$request->isMethod('delete')) {
            $this->makeResponse("Method Not Allowed", 405);
        }

        $this->auth($request);

        if (!$input = $request->getInput()) {
            $this->makeResponse("No input data", 422);
        }

        try {
            ( new \Api\V1\Hotel\Room())
                    ->delete($input);
            $this->makeResponse("Deleted");
        } catch (\PDOException $ex) {
            $this->makeResponse($ex->getMessage());
        }
    }

}
