<?php

namespace Core\Routing;

use Core\Http\Response;

abstract class Controller {

    private $response;

    protected function makeResponse($data, $code = 200) {

        return new Response($data, $code);
    }

    protected function auth(\Core\Http\Request $request) {

        $username = $request->get("PHP_AUTH_USER");
        $password = $request->get("PHP_AUTH_PW");
        $validUsername = 'user';
        $options = [
            'cost' => 11,
            'salt' =>"123456789012345678901" . chr(0),
        ];
        $validPass = '$2y$11$MTIzNDU2Nzg5MDEyMzQ1Ne7expXjNLoZGoKhDzmdJYpR/AYXXE.3m';
        
        if (!$username || $username != $validUsername || @password_hash($password, PASSWORD_BCRYPT,$options) != $validPass) {
            new Response("Unauthorized", 401);
        }
    }

}
