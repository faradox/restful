<?php

namespace Core\Routing;
use Core\Http\Response;

class Router {

	protected $request;

	public function handle() {

		$this->request = new \Core\Http\Request();

		$urlParameters = $this->request->getQueryString();


		if (empty($urlParameters))
			new Response("Not found", 404);

		list($version, $ns, $controller, $action,$param, $value) = array_pad(explode('/', $urlParameters,6),6,null);
                
                if (!$controller) {
                    new Response("Not found", 404);
                }
		$controllerName = ucfirst($controller)."Controller";
		if (!$action) {
			$action = 'index';
		}
		$ControllerNamespase = "\Api\\". ucfirst($version) . "\\".  ucfirst($ns) . "\\" . $controllerName ;
		
		if (!class_exists($ControllerNamespase)) {
			new Response("{$controllerName} does not exists!", 404);
		}
		if (!method_exists($ControllerNamespase, $action)) {
			new Response("{$controllerName} does not have {$action} method!", 404);
		}
		$object = new $ControllerNamespase();
		
		if (strstr($param, 'filter')) {
                    $param = substr($param, 8);
                    $param = preg_split('/(?=[A-Z])/',lcfirst($param));
                    $param = lcfirst($param[0]).'_'.lcfirst($param[1]);
                    
                    $value = [$param => $value];
                    
		}
		$object->$action($this->request,$value);
	}

}
