<?php

namespace Core\Database;

interface ConnectorInterface {

    public function connect($additional = null);
}
