<?php

namespace Core\Database;
use PDO;

/**
 * Connector for database
 */
class Connector implements ConnectorInterface {

    private $_connection;

    public function __construct($additional = null) {
        $this->connect($additional);
    }

    public function __destruct() {
        $this->disconnect();
    }

    public function connect($additional = null) {
        $connections = __DIR__ . '/../../config/database.php';

        $driver = null;

        if (file_exists($connections)) {
            $connection;
            $config = require $connections;
            if (is_array($config)) {

                if (array_key_exists('default', $config)) {
                    try {
                        if ($additional) {
                            if (array_key_exists($additional, $config['databases'])) {
                                $driver = $config['databases'][$additional];
                            } else {
                                throw new \Exception("Database configuration {$additional} not found", 404);
                            }
                        } else {
                            $driver = $config['databases'][$config['default']];
                        }

                        $charset = $driver['charset'];
                        $this->_connection = new PDO($driver['dsn'], $driver['username'], $driver['password']);

                        $this->_connection->setAttribute(PDO::ATTR_CASE, PDO::CASE_NATURAL);
                        $this->_connection->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
                        $this->_connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

                        $this->_connection->prepare("set names '$charset'")->execute();
                        return $this->_connection;
                    } catch (\PDOException $exception) {
                        echo "Connection error: " . $exception->getMessage();
                    }
                } else {
                    throw new \Exception("Default database not fount!", 404);
                }
            }
        } else {
            throw new \Exception("Database configuration not found", 404);
        }
    }

    public function getDefaultConnection() {
        return $this->_connection;
    }

    private function disconnect() {
        $this->_connection = null;
    }

}
