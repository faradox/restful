<?php

namespace Core\Database;

abstract class Model {

    protected $connection;
    protected $table;
    protected $fields = ['*'];
    protected $where = [];
    protected $limit;
    protected $order;

    public function __construct($db = null) {
        $connector = new Connector($db);
        $this->setConnection($connector);
    }

    public function setConnection(Connector $connector) {
        $this->connection = $connector;
    }

    public function getConnection() {
        return $this->connection->getDefaultConnection();
    }

    public function query() {

        try {
            $command = $this->getConnection()->query((string) $this);

            $result = [];
            while ($row = $command->fetch(\PDO::FETCH_ASSOC)) {
                array_push($result, $row);
            }

            return $result;
        } catch (\PDOException $ex) {
            return $ex->getMessage();
        }
    }

    public function select(array $fields) {
        $this->fields = $fields;

        return $this;
    }

    public function from(string $table) {
        $this->$table = $table;

        return $this;
    }

    public function where(array $condition) {

        $output = implode(' AND ', array_map(
                        function ($v, $k) {
                    return sprintf("%s='%s'", $k, $v);
                }, $condition, array_keys($condition)
        ));
        $this->where[] = " WHERE " . $output;

        return $this;
    }

    public function limit($limit) {
        $this->limit = " LIMIT {$limit}";

        return $this;
    }
    
    public function order(string $column) {
        $this->order = " ORDER BY {$column}";
        
        return $this;
        
    }

    


    public function insert($inputData) {


        $sql = "INSERT INTO {$this->table}";

        $sql .= " (`" . implode("`, `", array_keys($inputData)) . "`)";

        $values = join(', ', array_map(function ($value) {
                    return $value === null ? 'NULL' : ( strstr($value, 'ST_') !== false ? $value : "'$value'");
                }, $inputData));

        $sql .= " VALUES ($values) ";

        return $this->getConnection()->prepare($sql)->execute();
    }
    
    public function delete(array $condition) {
        $sql = "DELETE FROM {$this->table}";
        
        $output = implode(' AND ', array_map(
                        function ($v, $k) {
                    return sprintf("%s='%s'", $k, $v);
                }, $condition, array_keys($condition)
        ));
                
        $sql .= " WHERE " . $output;
        
        return $this->getConnection()->prepare($sql)->execute();
    }

    public function __toString() {

        return sprintf(
                'SELECT %s FROM %s %s %s', join(', ', $this->fields), $this->table, join(', ', $this->where), $this->limit
        );
    }

}
