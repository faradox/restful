<?php

namespace Core\Http;

use Core\Http\Request;

class Response {

    public $headers;
    public $request;
    protected $statusCode;

    public function __construct($data, $status = 200) {

        $request = new Request();
        $this->prepareResponse($request);

        $this->setStatusCode($status);
        $this->sendHeaders();

        if (strstr($this->headers, 'application/json')) {
            $this->wantsJson($data);
        } else if (strstr($this->headers, 'application/xml')) {
            $this->wantsXML($data);
        }
    }

    public function prepareResponse(Request $request) {
        $this->request = $request;

        $this->headers = $this->request->getAccept();
    }

    protected function wantsJson($data) {

        $response = [
            "code" => $this->statusCode,
            "status" => $this->statusCode,
            "data" => $data
        ];
        echo json_encode($response);
        exit;
    }

    protected function wantsXML($data) {

        $resonse = '<?xml version="1.0" encoding="UTF-8"?>'
                . '<response>'
                . '<code>' . $this->statusCode . '</code>'
                . '<status>' . $this->statusCode . '</status>';
        if (is_array($data)) {
            $resonse .= '<data>';

            foreach ($data as $key => $value) {
                if (is_array($value)) {
                    $resonse .= "<acc>";
                    foreach ($value as $key => $value) {
                        $resonse .= "<{$key}>";
                        $resonse .= $value;
                        $resonse .= "</{$key}>";
                    }

                    $resonse .= "</acc>";
                } else {
                    $resonse .= "<{$key}>";
                    $resonse .= $value;
                    $resonse .= "</{$key}>";
                }
            }

            $resonse .= "</data>";
        } else {
            $resonse .= "<data>{$data}</data>";
        }

        $resonse .= "</response>";

        echo $resonse;
        exit;
    }

    private function sendHeaders() {
        if ($this->headers) {
            header("Content-Type: " . $this->headers);
            header("HTTP/1.1 " . $this->statusCode . " " . $this->_requestStatusText($this->statusCode));
        }
    }

    private static function _requestStatusText($code) {
        $status = array(
            200 => 'OK',
            201 => 'Created',
            400 => 'Bad request',
            401 => 'Unauthorized!',
            404 => 'Not Found',
            405 => 'Method Not Allowed',
            500 => 'Internal Server Error',
        );
        return ($status[$code]) ? $status[$code] : $status[500];
    }

    public function setStatusCode($code = 200) {
        $this->statusCode = $code;
    }

}
