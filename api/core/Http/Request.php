<?php

namespace Core\Http;

class Request {

    private $contentType;
    private $accept;
    protected $method;
    protected $queryString;

    public function __construct() {
        $this->setAccept($_SERVER['HTTP_ACCEPT']);
        $this->setContentType(isset($_SERVER['CONTENT_TYPE']) ? $_SERVER['CONTENT_TYPE'] : null);
        $this->setMethod($_SERVER['REQUEST_METHOD']);
        $this->setQueryString($_SERVER['QUERY_STRING']);
    }

    public function setAccept($acceptHeader = '*/*') {
        $this->accept = $acceptHeader === '*/*' ? 'application/json' : $acceptHeader;
    }

    public function getAccept() {
        return $this->accept;
    }

    public function setContentType($contentType = null) {
        if (!$contentType) {
            $this->contentType = 'application/json';
        } else {
            $this->contentType = $contentType;
        }
    }
    
    public function get($param) {
        return array_key_exists($param,$_SERVER) ? $_SERVER[$param] : null;
    }

    public function getContentType() {
        return $this->contentType;
    }

    public function isMethod($method) {
        return $this->getMethod() === strtoupper($method);
    }

    public function setMethod($method) {
        $this->method = $method;
    }

    public function getMethod() {
        return $this->method;
    }

    public function setQueryString($query) {
        $this->queryString = $query;
    }

    public function getQueryString() {
        return rtrim($this->queryString, '/');
    }

    public function getInput($key = null) {
        $input = json_decode(file_get_contents("php://input"), true);
        return is_array($input) ? ($key ? $input[$key] : $input) : null;
    }

}
