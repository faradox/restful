<?php 
	return [
		'default' => 'mysql',

		'databases' => [
			'mysql' => [
				'dsn' => 'mysql:host=localhost;dbname=restapi',
				'username' => 'root',
				'password' => 'admin',
				'charset' => 'utf8'
			]
		]
	];